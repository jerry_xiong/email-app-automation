# README #

This is a simple automation framework for email/exchange app test on Android platform.

### What is this repository for? ###

* Be able to use EWS API within Android instrumentation test to prepare test data and assert server side.
* Be able to config client side test data in a dedicated properties file.
* Be able to integrate with my Android Monitor tool.
* Easy to create your own test cases for your email app.
* Easy to run single test or suite.
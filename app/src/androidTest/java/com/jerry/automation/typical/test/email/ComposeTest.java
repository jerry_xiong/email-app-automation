package com.jerry.automation.typical.test.email;

import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.util.Log;

import com.jerry.automation.framework.exchange.TestUtils;
import com.jerry.automation.framework.exchange.ServiceFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.search.FindItemsResults;

import static com.jerry.automation.framework.exchange.TestUtils.LAUNCH_TIMEOUT;
import static com.jerry.automation.framework.exchange.TestUtils.constructResourceId;
import static com.jerry.automation.framework.exchange.TestUtils.dismissExtra;
import static com.jerry.automation.framework.exchange.TestUtils.getCurrentTimeTag;
import static com.jerry.automation.framework.exchange.TestUtils.getDevice;
import static com.jerry.automation.framework.exchange.TestUtils.getTestData;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectByDesc;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectById;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectByText;
import static com.jerry.automation.framework.exchange.TestUtils.launchClient;
import static com.jerry.automation.framework.exchange.TestUtils.pullToRefresh;
import static com.jerry.automation.framework.exchange.TestUtils.unlockGDIfNecessary;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(AndroidJUnit4.class)
public class ComposeTest {

    @Before
    public void setUp() {
        launchClient();
        unlockGDIfNecessary();
    }

    @After
    public void tearDown() {
        getDevice().pressBack();
    }

    @Test
    public void composeEmail() {
        try {
            getDevice().wait(Until.findObject(By.res(constructResourceId("compose"))), LAUNCH_TIMEOUT * 2);
            getDevice().wait(Until.findObject(By.text("Inbox")), LAUNCH_TIMEOUT * 2);
            // verify we can see Inbox
            assertEquals("The Inbox hasn't been seen.", "Inbox", getUiObjectByText("Inbox").getText());
            getUiObjectById("compose").click();

            getUiObjectById("to").setText(getTestData("COMPOSE_TO_ADDRESS"));
            String subject = getCurrentTimeTag() + getTestData("COMPOSE_SUBJECT_PREFIX");
            String body = getTestData("COMPOSE_EMAIL_BODY");
            getUiObjectById("subject").setText(subject);
            getUiObjectById("body").setText(body);
            getUiObjectById("send").clickAndWaitForNewWindow(LAUNCH_TIMEOUT);

            // pull down to refresh email list
            UiScrollable listView = new UiScrollable(new UiSelector().resourceId("android:id/list"));
            assertNotNull(listView);
            // pull down to refresh will perform 2 way sync
            pullToRefresh();
            dismissExtra();

            // wait for a few seconds to receive new email on device
            // since the subject may be truncated in UI, so we use content description here.
            getDevice().wait(Until.findObject(By.descContains(subject)), LAUNCH_TIMEOUT * 5);
            assertTrue("Not found the email on device", getUiObjectByDesc(subject).exists());

            // verify new email existence via EWS API
            FindItemsResults<Item> searchResult = ServiceFactory.getExchangeService().findEmailItems(subject, body, WellKnownFolderName.Inbox);
            assertEquals(1, searchResult.getTotalCount());

        } catch (UiObjectNotFoundException e) {
            Log.e(TestUtils.TAG, "composeEmail test fail due to UIObject not found", e);
            fail("compose email test failed.\n" + e.getMessage());
        } catch (Exception e) {
            Log.e(TestUtils.TAG, "composeEmail test fail general error", e);
            fail("compose email test failed.\n" + e.getMessage());
        }
    }
}

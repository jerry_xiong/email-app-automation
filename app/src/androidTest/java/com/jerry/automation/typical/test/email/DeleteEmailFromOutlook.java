package com.jerry.automation.typical.test.email;
/**
 * Created by lqu on 10/22/2015.
 */
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.Until;
import android.util.Log;

import com.jerry.automation.framework.exchange.ServiceFactory;
import com.jerry.automation.framework.exchange.TestUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.enumeration.service.DeleteMode;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.search.FindItemsResults;

import static com.jerry.automation.framework.exchange.TestUtils.LAUNCH_TIMEOUT;
import static com.jerry.automation.framework.exchange.TestUtils.constructResourceId;
import static com.jerry.automation.framework.exchange.TestUtils.dismissExtra;
import static com.jerry.automation.framework.exchange.TestUtils.getCurrentTimeTag;
import static com.jerry.automation.framework.exchange.TestUtils.getDevice;
import static com.jerry.automation.framework.exchange.TestUtils.getTestData;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectByDesc;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectById;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectByText;
import static com.jerry.automation.framework.exchange.TestUtils.launchClient;
import static com.jerry.automation.framework.exchange.TestUtils.pullToRefresh;
import static com.jerry.automation.framework.exchange.TestUtils.unlockGDIfNecessary;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(AndroidJUnit4.class)
public class DeleteEmailFromOutlook {

    @Before
    public void setUp(){
        launchClient();
        unlockGDIfNecessary();
    }

    @After
    public void tearDown(){
        getDevice().pressBack();
    }

    @Test
    public void deleteEmailFromOutlook() {
        try {
            //Send email from device
            getDevice().wait(Until.findObject(By.res(constructResourceId("compose"))), LAUNCH_TIMEOUT * 2);
            getDevice().wait(Until.findObject(By.text("Inbox")), LAUNCH_TIMEOUT * 2);
            // verify we can see Inbox
            assertEquals("The Inbox hasn't been seen.", "Inbox", getUiObjectByText("Inbox").getText());
            getUiObjectById("compose").click();

            getUiObjectById("to").setText(getTestData("COMPOSE_TO_ADDRESS"));
            String subject = getCurrentTimeTag() + getTestData("COMPOSE_SUBJECT_PREFIX");
            String body = getTestData("COMPOSE_EMAIL_BODY");
            getUiObjectById("subject").setText(subject);
            getUiObjectById("body").setText(getTestData("COMPOSE_EMAIL_BODY"));
            getUiObjectById("send").clickAndWaitForNewWindow(LAUNCH_TIMEOUT);
            pullToRefresh();
            dismissExtra();

            // wait for a few seconds to receive new email
            getDevice().wait(Until.findObject(By.descContains(subject)), LAUNCH_TIMEOUT * 3);
            assertTrue("Not found the email on device", getUiObjectByDesc(subject).exists());

            // verify new email existence via EWS API
            FindItemsResults<Item> searchResult = ServiceFactory.getExchangeService().findEmailItems(subject, body, WellKnownFolderName.Inbox);
            assertEquals(1, searchResult.getTotalCount());

            //delete email from Outlook
            searchResult.getItems().get(0).delete(DeleteMode.MoveToDeletedItems);
            // wait for a few seconds verify the deletion sync to device
            //wait(LAUNCH_TIMEOUT * 5);
            getDevice().wait(Until.findObject(By.descContains(subject)), LAUNCH_TIMEOUT * 5);
            pullToRefresh();
            assertFalse("Not found the email on device", getUiObjectByDesc(subject).exists());

        } catch (UiObjectNotFoundException e) {
            Log.e(TestUtils.TAG, "delete email test fail due to UIObject not found", e);
            fail("delete email test failed.\n" + e.getMessage());
        } catch (Exception e) {
            Log.e(TestUtils.TAG, "delete email test fail", e);
            fail("delete email test failed.\n" + e.getMessage());
        }
    }

}

package com.jerry.automation.typical.suite;

import com.jerry.automation.typical.test.calendar.CreateMeetingFromDevice;
import com.jerry.automation.typical.test.email.ComposeTest;
import com.jerry.automation.typical.test.contact.CreateContactFromDevice;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ComposeTest.class,
        CreateContactFromDevice.class,
        CreateMeetingFromDevice.class
}
)
public class SmokeSuite {
}

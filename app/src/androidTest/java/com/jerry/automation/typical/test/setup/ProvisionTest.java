package com.jerry.automation.typical.test.setup;

import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.Until;
import android.view.KeyEvent;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;

import static com.jerry.automation.framework.exchange.TestUtils.LAUNCH_TIMEOUT;
import static com.jerry.automation.framework.exchange.TestUtils.constructResourceId;
import static com.jerry.automation.framework.exchange.TestUtils.getDevice;
import static com.jerry.automation.framework.exchange.TestUtils.getTestData;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectById;
import static com.jerry.automation.framework.exchange.TestUtils.launchClient;
import static com.jerry.automation.framework.exchange.TestUtils.unlockGDIfNecessary;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(AndroidJUnit4.class)
public class ProvisionTest {

    @Before
    public void setUp() {
        launchClient();
        unlockGDIfNecessary();
    }

    @Test
    public void provision() {
        try {
            gdProvision();
            setupMailbox();
        } catch (IOException e) {
            e.printStackTrace();
            fail("Parse access key failed.\n" + e.getMessage());
        } catch (UiObjectNotFoundException e) {
            e.printStackTrace();
            fail("provision failed.\n" + e.getMessage());
        }
    }

    private void gdProvision() throws UiObjectNotFoundException, IOException {
        // Input account email
        getUiObjectById("COM_GOOD_GD_EPROV_EMAIL_FIELD").setText(getTestData("ACCOUNT_EMAIL"));

        // press enter key
        getDevice().pressKeyCode(KeyEvent.KEYCODE_ENTER);

        // Input access key
        String[] accessKey = getTestData("ACCESS_KEY").split("-");
        getDevice().executeShellCommand("input text " + accessKey[0]);
        getDevice().executeShellCommand("input text " + accessKey[1]);
        getDevice().executeShellCommand("input text " + accessKey[2]);

        // press enter key
        getDevice().pressKeyCode(KeyEvent.KEYCODE_ENTER);

        // start GD Provision
        getDevice().wait(Until.hasObject(By.text("Retrieving Policies")), LAUNCH_TIMEOUT);

        // wait for a few seconds for GD provision
        getDevice().wait(Until.findObject(By.res(constructResourceId("COM_GOOD_GD_EPROV_SET_PWD_DLG_NEW_PWD_EDIT"))), LAUNCH_TIMEOUT * 5);

        // verify if GD provision complete
        assertTrue("Can't reach set password page. GD provision failed", getUiObjectById("COM_GOOD_GD_EPROV_SET_PWD_DLG_NEW_PWD_EDIT").exists());

        // Input new unlock password
        getUiObjectById("COM_GOOD_GD_EPROV_SET_PWD_DLG_NEW_PWD_EDIT").setText(getTestData("UNLOCK_PASSWORD"));
        getUiObjectById("COM_GOOD_GD_EPROV_SET_PWD_DLG_CONFIRM_PWD_EDIT").setText(getTestData("UNLOCK_PASSWORD"));
        getUiObjectById("COM_GOOD_GD_EPROV_ACCESS_BUTTON").clickAndWaitForNewWindow(LAUNCH_TIMEOUT);
    }

    private void setupMailbox() throws UiObjectNotFoundException {
        // verify if the account address has been filled.
        assertEquals("The account email hasn't been filled", getUiObjectById("account_email").getText(), getTestData("ACCOUNT_EMAIL"));

        // set account password
        getUiObjectById("account_password").setText(getTestData("ACCOUNT_PASSWORD"));

        // click next button
        getUiObjectById("next").clickAndWaitForNewWindow(LAUNCH_TIMEOUT);

        // start initial sync
        getDevice().wait(Until.hasObject(By.text("Validating server settings")), LAUNCH_TIMEOUT * 5);
        getDevice().wait(Until.hasObject(By.text("Creating account")), LAUNCH_TIMEOUT);
        getDevice().wait(Until.hasObject(By.textContains("Syncing")), LAUNCH_TIMEOUT);
        getDevice().wait(Until.findObject(By.text("Tap to navigate")), LAUNCH_TIMEOUT * 10);

        // dismissExtra the extra welcome image
        List<UiObject2> dismissImages = getDevice().findObjects(By.clazz("android.widget.ImageView"));
        if (dismissImages.size() > 0) {
            UiObject2 lastImage = dismissImages.get(dismissImages.size() - 1);
            lastImage.click();
        }

        // wait for a few seconds to complete initial sync
        getDevice().wait(Until.findObject(By.text("Inbox")), LAUNCH_TIMEOUT * 10);
    }
}

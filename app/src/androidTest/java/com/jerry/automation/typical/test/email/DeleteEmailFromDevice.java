package com.jerry.automation.typical.test.email;

/**
 * Created by lqu on 10/22/2015.
 */
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.Until;
import android.util.Log;

import com.jerry.automation.framework.exchange.IExchange;
import com.jerry.automation.framework.exchange.ServiceFactory;
import com.jerry.automation.framework.exchange.TestUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import microsoft.exchange.webservices.data.core.enumeration.property.Importance;
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.property.complex.MessageBody;
import microsoft.exchange.webservices.data.search.FindItemsResults;

import static com.jerry.automation.framework.exchange.TestUtils.LAUNCH_TIMEOUT;
import static com.jerry.automation.framework.exchange.TestUtils.getCurrentTimeTag;
import static com.jerry.automation.framework.exchange.TestUtils.getDevice;
import static com.jerry.automation.framework.exchange.TestUtils.getTestData;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectByDesc;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectById;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectByText;
import static com.jerry.automation.framework.exchange.TestUtils.launchClient;
import static com.jerry.automation.framework.exchange.TestUtils.pullToRefresh;
import static com.jerry.automation.framework.exchange.TestUtils.unlockGDIfNecessary;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
@RunWith(AndroidJUnit4.class)
public class DeleteEmailFromDevice {

    private IExchange exchange = null;

    @Before
    public void setUp(){
        launchClient();
        unlockGDIfNecessary();
        exchange = ServiceFactory.getExchangeService();
    }

    @After
    public void tearDown(){
        getDevice().pressBack();
    }

    @Test
    public void deleteEmailFromDevice() {
        try {
            //Send email from Outlook
            String subject = "EWS Android Testing " + getCurrentTimeTag();
            String body = "Sent using EWS Java API";
            EmailMessage emailMessage = exchange.createNewEmail();
            emailMessage.setSubject(subject);
            emailMessage.setBody(new MessageBody(body));
            emailMessage.getToRecipients().add(getTestData("ACCOUNT_EMAIL"));
            emailMessage.setImportance(Importance.Low);
            emailMessage.save();
            emailMessage.send();
            Thread.sleep(TestUtils.LAUNCH_TIMEOUT * 3);

            FindItemsResults<Item> searchResult = exchange.findEmailItems(subject, body, WellKnownFolderName.Inbox);
            assertEquals(1, searchResult.getTotalCount());

            // verify we can see Inbox on device
            assertEquals("The Inbox hasn't been seen.", "Inbox", getUiObjectByText("Inbox").getText());
            // pull to refresh to receive new email
            pullToRefresh();
            getDevice().wait(Until.findObject(By.descContains(subject)), LAUNCH_TIMEOUT * 5);
            assertTrue("Not found the email on device", getUiObjectByDesc(subject).exists());

            //delete email on device
            getUiObjectByDesc(subject).click();
            getUiObjectById("delete").click();
            getDevice().pressBack();

            // verify the email existence via EWS API
            Thread.sleep(TestUtils.LAUNCH_TIMEOUT * 5);
            searchResult = exchange.findEmailItems(subject, body, WellKnownFolderName.Inbox);
            assertEquals(0, searchResult.getTotalCount());

            searchResult = exchange.findEmailItems(subject, body, WellKnownFolderName.DeletedItems);
            assertEquals(1, searchResult.getTotalCount());

        } catch (UiObjectNotFoundException e) {
            Log.e(TestUtils.TAG, "delete email test fail due to UIObject not found", e);
            fail("delete email test failed.\n" + e.getMessage());
        } catch (Exception e) {
            Log.e(TestUtils.TAG, "delete email test failed", e);
            fail("delete email test failed\n" + e.getMessage());
        }
    }
}


package com.jerry.automation.typical.test.calendar;

/**
 * Created by lqu on 10/28/2015.
 */

import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.Until;
import android.text.format.DateUtils;
import android.util.Log;

import com.jerry.automation.framework.exchange.IExchange;
import com.jerry.automation.framework.exchange.ServiceFactory;
import com.jerry.automation.framework.exchange.TestUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.SimpleDateFormat;
import java.util.List;

import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.search.FindItemsResults;

import static com.jerry.automation.framework.exchange.TestUtils.LAUNCH_TIMEOUT;
import static com.jerry.automation.framework.exchange.TestUtils.constructResourceId;
import static com.jerry.automation.framework.exchange.TestUtils.getCurrentTimeTag;
import static com.jerry.automation.framework.exchange.TestUtils.getDevice;
import static com.jerry.automation.framework.exchange.TestUtils.getTestData;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectById;
import static com.jerry.automation.framework.exchange.TestUtils.launchClient;
import static com.jerry.automation.framework.exchange.TestUtils.unlockGDIfNecessary;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(AndroidJUnit4.class)
public class CreateMeetingFromDevice {

    private IExchange exchange = null;
    private FindItemsResults<Item> findResults = null;
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    private String meetingTitle = null;
    private String meetingLocation = null;
    @Before
    public void setUp() {
        launchClient();
        unlockGDIfNecessary();
        exchange = ServiceFactory.getExchangeService();
        meetingTitle = getCurrentTimeTag() + "MR";
        meetingLocation = "Meeting Room";
    }

    @After
    public void tearDown() {
//        try {
//            findResults.getItems().get(0).delete(DeleteMode.HardDelete);
//        } catch (Exception e) {
//            Assert.fail("tear down failed " + e.getMessage());
//        }
        getDevice().pressBack();
    }

    @Test
    public void createMeetingFromDevice(){
        try{
            getDevice().wait(Until.findObject(By.res(constructResourceId("launchpad_button_compat"))), LAUNCH_TIMEOUT * 2);
            //verify we can see launchpad
            assertTrue("The launchpad hasn't been seen.", getUiObjectById("launchpad_button_compat").exists());

            //create contact from device
            getUiObjectById("launchpad_button_compat").click();
//            getDevice().findObject(new UiSelector().resourceId("text").text("Contacts")).click();
            List<UiObject2> launchpadEntryList = getDevice().findObjects(By.res(constructResourceId("app_item_layout")));
            UiObject2 calendarEntry = launchpadEntryList.get(1);
            assertTrue("launchpad elements are not correct.", launchpadEntryList.size() == 4);
            calendarEntry.click();
            getDevice().wait(Until.findObject(By.res(constructResourceId("action_create_event"))), LAUNCH_TIMEOUT);
            getUiObjectById("action_create_event").clickAndWaitForNewWindow();

            if (getUiObjectById("title").exists()) {
                getUiObjectById("title").setText(meetingTitle);
                getUiObjectById("location").setText(meetingLocation);
                getUiObjectById("start_date").setText(getCurrentTimeTag());
                getUiObjectById("end_date").setText(dateFormat.format(System.currentTimeMillis() + DateUtils.HOUR_IN_MILLIS));
                getUiObjectById("attendees_required").setText(getTestData("COMPOSE_TO_ADDRESS"));
                getUiObjectById("action_done").click();
                //verify the MR synced to Outlook
                Thread.sleep(LAUNCH_TIMEOUT * 10);
                findResults = exchange.findCalendarItems(meetingTitle, meetingLocation);
                assertEquals("calendar not found", 1, findResults.getTotalCount());
            } else {
                fail("test failed due to calendar title UIObject not found");
            }
        }catch (UiObjectNotFoundException e) {
            Log.e(TestUtils.TAG, "test fail due to UIObject not found", e);
            fail("test failed.\n" + e.getMessage());
        } catch (Exception e) {
            Log.e(TestUtils.TAG, "create contact test fail", e);
            fail("test failed.\n" + e.getMessage());
        }

    }
}

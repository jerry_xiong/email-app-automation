package com.jerry.automation.typical.test.email;
/**
 * Created by lqu on 10/22/2015.
 */
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.Until;
import android.util.Log;

import com.jerry.automation.framework.exchange.IExchange;
import com.jerry.automation.framework.exchange.ServiceFactory;
import com.jerry.automation.framework.exchange.TestUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import microsoft.exchange.webservices.data.core.enumeration.property.Importance;
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.enumeration.service.ConflictResolutionMode;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.property.complex.MessageBody;
import microsoft.exchange.webservices.data.search.FindItemsResults;

import static com.jerry.automation.framework.exchange.TestUtils.LAUNCH_TIMEOUT;
import static com.jerry.automation.framework.exchange.TestUtils.getCurrentTimeTag;
import static com.jerry.automation.framework.exchange.TestUtils.getDevice;
import static com.jerry.automation.framework.exchange.TestUtils.getTestData;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectByDesc;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectById;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectByText;
import static com.jerry.automation.framework.exchange.TestUtils.pullToRefresh;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(AndroidJUnit4.class)
public class MarkUnreadFromDevice {

    private IExchange exchange = null;

    @Before
    public void setUp() {
        exchange = ServiceFactory.getExchangeService();
    }

    @After
    public void tearDown() { getDevice().pressBack(); }

    @Test
    public void markUnreadFromDevice(){
        try{
            getDevice().wait(Until.findObject(By.text("Inbox")), LAUNCH_TIMEOUT * 2);
            // verify we can see Inbox
            assertEquals("The Inbox hasn't been seen.", "Inbox", getUiObjectByText("Inbox").getText());

            //Send email from Outlook
            String subject = "EWS Android Testing " + getCurrentTimeTag();
            String body = "Sent using EWS Java API";
            EmailMessage emailMessage = exchange.createNewEmail();
            emailMessage.setSubject(subject);
            emailMessage.setBody(MessageBody.getMessageBodyFromText(body));
            emailMessage.getToRecipients().add(getTestData("ACCOUNT_EMAIL"));
            emailMessage.setImportance(Importance.Low);
            emailMessage.save();
            emailMessage.send();
            Thread.sleep(TestUtils.LAUNCH_TIMEOUT * 3);
            //mark read on Outlook
            FindItemsResults searchResult = exchange.findEmailItems(subject, body, WellKnownFolderName.Inbox);
            assertTrue(searchResult.getTotalCount() == 1);
            EmailMessage emailMessage1 = (EmailMessage) searchResult.getItems().get(0);
            assertFalse(emailMessage1.getIsRead());
            emailMessage1.setIsRead(true);
            emailMessage1.update(ConflictResolutionMode.AlwaysOverwrite);

            //check device read email and mark unread
            Thread.sleep(LAUNCH_TIMEOUT * 3);
            pullToRefresh();
            assertTrue("Not found read email on device", getUiObjectByDesc(subject).getContentDescription().contains("conversation read"));
            getUiObjectByDesc(subject).click();
            getUiObjectById("inside_conversation_unread").click();

            //verify the email marked as unread on Outlook
            Thread.sleep(LAUNCH_TIMEOUT * 2);
            assertTrue(emailMessage1.getIsRead());

        } catch (UiObjectNotFoundException e) {
            Log.e(TestUtils.TAG, "test fail due to UIObject not found", e);
            fail("test failed.\n" + e.getMessage());
        } catch (Exception e) {
            Log.e(TestUtils.TAG, "mark unread email test fail", e);
            fail("test failed.\n" + e.getMessage());
        }
    }
}

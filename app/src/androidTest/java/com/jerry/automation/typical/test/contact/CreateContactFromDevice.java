package com.jerry.automation.typical.test.contact;

/**
 * Created by lqu on 10/28/2015.
 */

import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.util.Log;

import com.jerry.automation.framework.exchange.IExchange;
import com.jerry.automation.framework.exchange.ServiceFactory;
import com.jerry.automation.framework.exchange.TestUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.search.FindItemsResults;

import static com.jerry.automation.framework.exchange.TestUtils.LAUNCH_TIMEOUT;
import static com.jerry.automation.framework.exchange.TestUtils.constructResourceId;
import static com.jerry.automation.framework.exchange.TestUtils.getCurrentTimeTag;
import static com.jerry.automation.framework.exchange.TestUtils.getDevice;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectById;
import static com.jerry.automation.framework.exchange.TestUtils.launchClient;
import static com.jerry.automation.framework.exchange.TestUtils.unlockGDIfNecessary;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(AndroidJUnit4.class)
public class CreateContactFromDevice {

    private IExchange exchange = null;

    private FindItemsResults<Item> findResults = null;

    private String firstName = null;

    @Before
    public void setUp() {
        launchClient();
        unlockGDIfNecessary();
        exchange = ServiceFactory.getExchangeService();
        firstName = getCurrentTimeTag() + "Lucy";
    }

    @After
    public void tearDown() {
//        try {
//            assertNotNull(findResults);
//            assertTrue(findResults.getTotalCount() > 0);
//            Item item = findResults.getItems().get(0);
//            item.delete(DeleteMode.HardDelete);
//            assertEquals(0, exchange.findContactItems(firstName, "Test").getTotalCount());
//            getDevice().pressBack();
//            getDevice().pressBack();
//        } catch (Exception e) {
//            Assert.fail("tear down failed " + e.getMessage());
//        }
        getDevice().pressBack();
    }

    @Test
    public void createContactFromDevice() {
        try {
            getDevice().wait(Until.findObject(By.res(constructResourceId("launchpad_button_compat"))), LAUNCH_TIMEOUT * 2);
            //verify we can see launchpad
            assertTrue("The launchpad hasn't been seen.", getUiObjectById("launchpad_button_compat").exists());

            //create contact from device
            getUiObjectById("launchpad_button_compat").click();
            List<UiObject2> launchpadEntryList = getDevice().findObjects(By.res(constructResourceId("app_item_layout")));
            assertTrue("launchpad elements are not correct.", launchpadEntryList.size() == 4);
            UiObject2 contactEntry = launchpadEntryList.get(2);
            contactEntry.click();
            getDevice().wait(Until.findObject(By.res(constructResourceId("menu_add_contact"))), LAUNCH_TIMEOUT );
            getUiObjectById("menu_add_contact").clickAndWaitForNewWindow();
            getDevice().pressBack();

            UiObject givenName = getDevice().findObject(new UiSelector().className("android.widget.EditText").text("Given name"));
            if (givenName.exists()) {
                givenName.setText(firstName);
                getDevice().findObject(new UiSelector().className("android.widget.EditText").text("Family name")).setText("Test");
                getDevice().findObject(new UiSelector().className("android.widget.EditText").text("Email")).setText("lqu2@g3.qagood.com");
                getDevice().findObject(new UiSelector().className("android.widget.EditText").text("Phone")).setText("123456");
                getUiObjectById("menu_done").click();
                getDevice().pressBack();
                //            getUiObjectByDesc("More options").click();
//            getUiObjectById("title").click();

                //verify the contact synced to Outlook
                Thread.sleep(LAUNCH_TIMEOUT * 25);
                findResults = exchange.findContactItems(firstName, "Test");
                assertEquals("contact not found", 1, findResults.getTotalCount());
            } else {
                fail("test failed due to Given name title UIObject not found");
            }
        } catch (UiObjectNotFoundException e) {
            Log.e(TestUtils.TAG, "test fail due to UIObject not found", e);
            fail("test failed.\n" + e.getMessage());
        } catch (Exception e) {
            Log.e(TestUtils.TAG, "create contact test fail", e);
            fail("test failed.\n" + e.getMessage());
        }
    }
}
package com.jerry.automation.typical.test.email;

import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.util.Log;

import com.jerry.automation.framework.exchange.TestUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.jerry.automation.framework.exchange.TestUtils.LAUNCH_TIMEOUT;
import static com.jerry.automation.framework.exchange.TestUtils.getDevice;
import static com.jerry.automation.framework.exchange.TestUtils.getTestData;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectById;
import static com.jerry.automation.framework.exchange.TestUtils.launchClient;
import static com.jerry.automation.framework.exchange.TestUtils.unlockGDIfNecessary;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(AndroidJUnit4.class)
public class SearchTest {

    @Before
    public void setUp() {
        launchClient();
        unlockGDIfNecessary();
    }

    @After
    public void tearDown() {
        // dismissExtra keyboard
        getDevice().pressBack();
        // back to last page
        getDevice().pressBack();
    }

    @Test
    public void searchEmails() {
        try {
            // start a simple search
            getUiObjectById("search").click();
            getUiObjectById("android:id/search_src_text", false).setText(getTestData("SEARCH_SUBJECT"));
            // wait for a few seconds for local search
            getDevice().wait(Until.findObject(By.text(getTestData("SEARCH_SUBJECT"))), LAUNCH_TIMEOUT);

                    // check search result
            UiScrollable listView = new UiScrollable(new UiSelector().resourceId("android:id/list"));
            assertTrue(listView.getChildCount() >= 1);

            // enter advanced search
            getUiObjectById("specific_advanced_search_button").clickAndWaitForNewWindow(LAUNCH_TIMEOUT);

            // dismissExtra the keyboard
            getDevice().pressBack();

            UiObject contains = getUiObjectById("search_contains");
            // check the query mapping
            assertEquals(getTestData("SEARCH_SUBJECT"), contains.getText());

            // set a value on from
            getUiObjectById("search_from").setText(getTestData("SEARCH_FROM"));

                    // click search period dropdown
            getDevice().findObject(new UiSelector().className("android.widget.CheckedTextView").text(getTestData("SEARCH_PERIOD_ALL"))).click();
                    // select Today as the period
            getDevice().findObject(new UiSelector().className("android.widget.CheckedTextView").text(getTestData("SEARCH_PERIOD_TODAY"))).click();

                    // apply advanced search
            getUiObjectById("advanced_search_confirm").click();

            // wait for a few seconds for local search
            getDevice().wait(Until.findObject(By.text(getTestData("SEARCH_SUBJECT"))), LAUNCH_TIMEOUT);

            // check search result
            listView = new UiScrollable(new UiSelector().resourceId("android:id/list"));
            assertTrue(listView.exists());
            assertTrue(listView.getChildCount() >= 1);
            assertEquals("The search query is not correct.", getTestData("SEARCH_QUERY"), getUiObjectById("android:id/search_src_text", false).getText());
        } catch (UiObjectNotFoundException e) {
            Log.e(TestUtils.TAG, "search test fail due to UIObject not found", e);
            fail("search test failed.\n" + e.getMessage());
        }
    }

}

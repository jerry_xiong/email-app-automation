package com.jerry.automation.typical.test.email;
/**
 * Created by lqu on 10/20/2015.
 */
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.util.Log;

import com.jerry.automation.framework.exchange.ServiceFactory;
import com.jerry.automation.framework.exchange.TestUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.search.FindItemsResults;

import static com.jerry.automation.framework.exchange.TestUtils.LAUNCH_TIMEOUT;
import static com.jerry.automation.framework.exchange.TestUtils.constructResourceId;
import static com.jerry.automation.framework.exchange.TestUtils.dismissExtra;
import static com.jerry.automation.framework.exchange.TestUtils.getCurrentTimeTag;
import static com.jerry.automation.framework.exchange.TestUtils.getDevice;
import static com.jerry.automation.framework.exchange.TestUtils.getTestData;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectByDesc;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectById;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectByText;
import static com.jerry.automation.framework.exchange.TestUtils.launchClient;
import static com.jerry.automation.framework.exchange.TestUtils.pullToRefresh;
import static com.jerry.automation.framework.exchange.TestUtils.unlockGDIfNecessary;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(AndroidJUnit4.class)
public class ForwardEmailTest {

    @Before
    public void setup(){
        launchClient();
        unlockGDIfNecessary();
    }

    @After
    public void teardown(){
        getDevice().pressBack();
    }
    @Test
    public void forwardEmailTest(){
        //Send and Received an email
        try {
            getDevice().wait(Until.findObject(By.res(constructResourceId("compose"))), LAUNCH_TIMEOUT * 2);
            getDevice().wait(Until.findObject(By.text("Inbox")), LAUNCH_TIMEOUT * 2);
            // verify we can see Inbox
            assertEquals("The Inbox hasn't been seen.", "Inbox", getUiObjectByText("Inbox").getText());
            getUiObjectById("compose").click();

            getUiObjectById("to").setText(getTestData("COMPOSE_TO_ADDRESS"));
            String subject = getCurrentTimeTag() + getTestData("COMPOSE_SUBJECT_PREFIX");
            getUiObjectById("subject").setText(subject);
            getUiObjectById("body").setText(getTestData("COMPOSE_EMAIL_BODY"));
            getUiObjectById("send").clickAndWaitForNewWindow(LAUNCH_TIMEOUT);

            // pull down to refresh email list
            pullToRefresh();
            // dismissExtra the extra item if necessary
            dismissExtra();

            // wait for a few seconds to receive new email
            getDevice().wait(Until.findObject(By.descContains(subject)), LAUNCH_TIMEOUT * 8);
            assertTrue("Not found the email on device", getUiObjectByDesc(subject).exists());

            //open and forward email
            getUiObjectByDesc(subject).click();
            getUiObjectById("overflow").click();
            getDevice().findObject(new UiSelector().className("android.widget.TextView").text("Forward")).click();
            getDevice().pressBack();
            getUiObjectById("to").setText(getTestData("FORWARD_TO_ADDRESS"));
            getUiObjectById("body").setText(getTestData("FORWARD_EMAIL_BODY"));
            getUiObjectById("send").clickAndWaitForNewWindow(LAUNCH_TIMEOUT);
            getDevice().pressBack();
            Thread.sleep(LAUNCH_TIMEOUT * 3);

            //pull down to refresh email list
            pullToRefresh();
            getDevice().wait(Until.findObject(By.descContains("Fwd: " + subject)), LAUNCH_TIMEOUT * 5);
            // check forward email on device
            assertTrue("Not found the forward email on device", getUiObjectByDesc("Fwd: " + subject).exists());
            //check reply email on Outlook
            FindItemsResults<Item> serverResult = ServiceFactory.getExchangeService().
                    findEmailItems(subject, getTestData("FORWARD_EMAIL_BODY"), WellKnownFolderName.Inbox);
            assertEquals("verify server email failed ", 1, serverResult.getTotalCount());

        } catch (UiObjectNotFoundException e) {
            Log.e(TestUtils.TAG, "forward test fail due to UIObject not found", e);
            fail("forward email test failed.\n" + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TestUtils.TAG, "forward test fail", e);
            fail("forward email test failed.\n" + e.getMessage());
        }
    }

}

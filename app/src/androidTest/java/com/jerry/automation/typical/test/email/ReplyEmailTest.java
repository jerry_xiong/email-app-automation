package com.jerry.automation.typical.test.email;
/**
 * Created by lqu on 10/15/2015.
 */
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.Until;
import android.util.Log;

import com.jerry.automation.framework.exchange.TestUtils;
import com.jerry.automation.framework.exchange.ServiceFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.search.FindItemsResults;

import static com.jerry.automation.framework.exchange.TestUtils.LAUNCH_TIMEOUT;
import static com.jerry.automation.framework.exchange.TestUtils.constructResourceId;
import static com.jerry.automation.framework.exchange.TestUtils.dismissExtra;
import static com.jerry.automation.framework.exchange.TestUtils.getCurrentTimeTag;
import static com.jerry.automation.framework.exchange.TestUtils.getDevice;
import static com.jerry.automation.framework.exchange.TestUtils.getTestData;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectByDesc;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectById;
import static com.jerry.automation.framework.exchange.TestUtils.getUiObjectByText;
import static com.jerry.automation.framework.exchange.TestUtils.launchClient;
import static com.jerry.automation.framework.exchange.TestUtils.pullToRefresh;
import static com.jerry.automation.framework.exchange.TestUtils.unlockGDIfNecessary;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(AndroidJUnit4.class)
public class ReplyEmailTest {

    @Before
    public void setUp(){
        launchClient();
        unlockGDIfNecessary();
    }

    @After
    public void tearDown(){
        getDevice().pressBack();
    }

    @Test
    public void replyEmailTest(){
        try {
            //Send and Received an email
            getDevice().wait(Until.findObject(By.res(constructResourceId("compose"))), LAUNCH_TIMEOUT * 2);
            getDevice().wait(Until.findObject(By.text("Inbox")), LAUNCH_TIMEOUT * 2);
            // verify we can see Inbox
            assertEquals("The Inbox hasn't been seen.", "Inbox", getUiObjectByText("Inbox").getText());
            getUiObjectById("compose").click();

            getUiObjectById("to").setText(getTestData("COMPOSE_TO_ADDRESS"));
            String subject = getCurrentTimeTag() + getTestData("COMPOSE_SUBJECT_PREFIX");
            getUiObjectById("subject").setText(subject);
            getUiObjectById("body").setText(getTestData("COMPOSE_EMAIL_BODY"));
                    getUiObjectById("send").clickAndWaitForNewWindow(LAUNCH_TIMEOUT);
            pullToRefresh();
            dismissExtra();

            // wait for a few seconds to receive new email
            getDevice().wait(Until.findObject(By.descContains(subject)), LAUNCH_TIMEOUT * 6);
            assertTrue("Not found the email on device", getUiObjectByDesc(subject).exists());

            //open and reply email
            getUiObjectByDesc(subject).click();
            getUiObjectById("reply").click();
            getUiObjectById("body").setText(getTestData("REPLY_EMAIL_BODY"));
            getUiObjectById("send").clickAndWaitForNewWindow(LAUNCH_TIMEOUT);
            getDevice().pressBack();
            pullToRefresh();

            // wait for a few seconds to receive new email
//            String subject = "1445333994306Lucy GW Test";
            //getDevice().wait(Until.findObject(By.text("Re: " + subject)), LAUNCH_TIMEOUT);
            //getUiObjectByDesc("Re: " + subject).clickAndWaitForNewWindow(LAUNCH_TIMEOUT);
            // check reply email body
//            assertTrue("Not found the target body",
//            getDevice().findObject(new UiSelector().textContains(getTestData(R.string.REPLY_EMAIL_BODY))).exists());

            // wait for a few seconds to receive new email
            getDevice().wait(Until.findObject(By.descContains("Re: " + subject)), LAUNCH_TIMEOUT * 6);
            //check reply email on device
            assertTrue("Not found the reply email on device", getUiObjectByDesc("Re: " + subject).exists());
            //check reply email on Outlook
            FindItemsResults<Item> serverResult = ServiceFactory.getExchangeService()
                    .findEmailItems("Re: " + subject, getTestData("REPLY_EMAIL_BODY"), WellKnownFolderName.Inbox);
            assertEquals("verify server email failed ", 1, serverResult.getTotalCount());

        } catch (UiObjectNotFoundException e) {
            Log.e(TestUtils.TAG, "reply test fail due to UIObject not found", e);
            fail("reply email test failed.\n" + e.getMessage());
        } catch (Exception e) {
            Log.e(TestUtils.TAG, "reply test fail", e);
            fail("reply email test failed.\n" + e.getMessage());
        }
    }
}

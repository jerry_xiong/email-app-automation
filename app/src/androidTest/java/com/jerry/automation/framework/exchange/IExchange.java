package com.jerry.automation.framework.exchange;

import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.service.item.Appointment;
import microsoft.exchange.webservices.data.core.service.item.Contact;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.search.FindFoldersResults;
import microsoft.exchange.webservices.data.search.FindItemsResults;
import microsoft.exchange.webservices.data.search.filter.SearchFilter;
import microsoft.exchange.webservices.data.sync.ChangeCollection;
import microsoft.exchange.webservices.data.sync.FolderChange;

/**
 * Created by jexiong on 10/19/2015.
 */
public interface IExchange {

    void createNewFolder(String displayName, WellKnownFolderName parentFolder) throws Exception;

    FindFoldersResults findChildFolders(WellKnownFolderName folderName) throws Exception;

    ChangeCollection<FolderChange> startSync(WellKnownFolderName folderName) throws Exception;

    FindItemsResults<Item> findEmailItems(String subject, String body, WellKnownFolderName folderName) throws Exception;

    FindItemsResults<Item> findEmailItems(SearchFilter searchFilter, WellKnownFolderName folderName) throws Exception;

    FindItemsResults<Item> findContactItems(SearchFilter searchFilter) throws Exception;

    FindItemsResults<Item> findContactItems(String firstName, String lastName) throws Exception;

    FindItemsResults<Item> findCalendarItems(String subject, String location) throws Exception;

    FindItemsResults<Item> findCalendarItems(SearchFilter searchFilter) throws Exception;

    FindItemsResults<Item> findItems(WellKnownFolderName folderName) throws Exception;

    Contact createNewContact() throws Exception;

    Appointment createNewAppointment() throws Exception;

    EmailMessage createNewEmail() throws Exception;

    ExchangeService getRawExchangeService();

}

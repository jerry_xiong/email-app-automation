package com.jerry.automation.framework.exchange;

import java.net.URI;

import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.PropertySet;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.enumeration.property.BasePropertySet;
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.enumeration.search.LogicalOperator;
import microsoft.exchange.webservices.data.core.enumeration.search.SortDirection;
import microsoft.exchange.webservices.data.core.service.folder.Folder;
import microsoft.exchange.webservices.data.core.service.item.Appointment;
import microsoft.exchange.webservices.data.core.service.item.Contact;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.core.service.schema.AppointmentSchema;
import microsoft.exchange.webservices.data.core.service.schema.ContactSchema;
import microsoft.exchange.webservices.data.core.service.schema.ItemSchema;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.misc.IAsyncResult;
import microsoft.exchange.webservices.data.property.complex.FolderId;
import microsoft.exchange.webservices.data.search.FindFoldersResults;
import microsoft.exchange.webservices.data.search.FindItemsResults;
import microsoft.exchange.webservices.data.search.FolderView;
import microsoft.exchange.webservices.data.search.ItemView;
import microsoft.exchange.webservices.data.search.filter.SearchFilter;
import microsoft.exchange.webservices.data.sync.ChangeCollection;
import microsoft.exchange.webservices.data.sync.FolderChange;

/**
 * Refer to https://github.com/OfficeDev/ews-java-api/wiki/Getting-Started-Guide#sending-a-message
 * to extend the service.
 */
public class EwsService implements IExchange {

    private static IExchange INSTANCE = new EwsService();

    private ExchangeService service;

    protected static IExchange getInstance() {
        return INSTANCE;
    }

    private EwsService() {
        init();
    }

    private void init() {
        service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
        String account = TestUtils.getTestData("ACCOUNT_EMAIL");
        String password = TestUtils.getTestData("ACCOUNT_PASSWORD");
        ExchangeCredentials credentials = new WebCredentials(account, password);
        service.setCredentials(credentials);
        String url = TestUtils.getTestData("EXCHANGE_URL");
        service.setUrl(URI.create(url));
    }

    @Override
    public void createNewFolder(String displayName, WellKnownFolderName parentFolder) throws Exception {
        Folder newFolder = new Folder(service);
        newFolder.setDisplayName(displayName);
        newFolder.save(parentFolder);
    }

    @Override
    public FindFoldersResults findChildFolders(WellKnownFolderName folderName) throws Exception {
        return service.findFolders(folderName, new FolderView(Integer.MAX_VALUE));
    }

    @Override
    public ChangeCollection<FolderChange> startSync(WellKnownFolderName folderName) throws Exception {
        FolderId folderId = new FolderId(folderName);
        IAsyncResult asyncResult;
        asyncResult = service
                .beginSyncFolderHierarchy(null, null, folderId, PropertySet.FirstClassProperties, null);
        return service.endSyncFolderHierarchy(asyncResult);
    }

    @Override
    public FindItemsResults<Item> findEmailItems(SearchFilter searchFilter, WellKnownFolderName folderName) throws Exception {
        ItemView view = new ItemView(10);
        view.getOrderBy().add(ItemSchema.DateTimeReceived, SortDirection.Ascending);
        view.setPropertySet(new PropertySet(BasePropertySet.FirstClassProperties));
        return service.findItems(folderName, searchFilter, view);
    }

    @Override
    public FindItemsResults<Item> findEmailItems(String subject, String body, WellKnownFolderName folderName) throws Exception {
        return findEmailItems(
                new SearchFilter.SearchFilterCollection(LogicalOperator.And,
                        new SearchFilter.ContainsSubstring(ItemSchema.Subject, subject),
                        new SearchFilter.ContainsSubstring(ItemSchema.Body, body)), folderName);
    }

    @Override
    public FindItemsResults<Item> findContactItems(SearchFilter searchFilter) throws Exception {
        ItemView view = new ItemView(10);
        view.getOrderBy().add(ItemSchema.DateTimeReceived, SortDirection.Ascending);
        view.setPropertySet(new PropertySet(BasePropertySet.IdOnly, ItemSchema.Subject, ItemSchema.DateTimeReceived));
        return service.findItems(WellKnownFolderName.Contacts, searchFilter, view);
    }

    @Override
    public FindItemsResults<Item> findContactItems(String firstName, String lastName) throws Exception {
        return findContactItems(
                new SearchFilter.SearchFilterCollection(LogicalOperator.And,
                        new SearchFilter.ContainsSubstring(ContactSchema.GivenName, firstName),
                        new SearchFilter.ContainsSubstring(ContactSchema.Surname, lastName)));
    }

    @Override
    public FindItemsResults<Item> findCalendarItems(String subject, String location) throws Exception {
        return findCalendarItems(
                new SearchFilter.SearchFilterCollection(LogicalOperator.And,
                        new SearchFilter.ContainsSubstring(AppointmentSchema.Subject, subject),
                        new SearchFilter.ContainsSubstring(AppointmentSchema.Location, location)));
    }

    @Override
    public FindItemsResults<Item> findCalendarItems(SearchFilter searchFilter) throws Exception {
        ItemView view = new ItemView(10);
        view.getOrderBy().add(ItemSchema.DateTimeReceived, SortDirection.Ascending);
        view.setPropertySet(new PropertySet(BasePropertySet.IdOnly, ItemSchema.Subject, ItemSchema.DateTimeReceived));
        return service.findItems(WellKnownFolderName.Calendar, searchFilter, view);
    }

    @Override
    public FindItemsResults<Item> findItems(WellKnownFolderName folderName) throws Exception {
        ItemView view = new ItemView(100);
        FindItemsResults<Item> findResults;
        do {
            findResults = service.findItems(folderName, view);
            view.setOffset(view.getOffset() + 10);
        } while (findResults.isMoreAvailable());
        return findResults;
    }

    @Override
    public Contact createNewContact() throws Exception {
        return new Contact(service);
    }

    @Override
    public Appointment createNewAppointment() throws Exception {
        return new Appointment(service);
    }

    @Override
    public EmailMessage createNewEmail() throws Exception {
        return new EmailMessage(service);
    }

    @Override
    public ExchangeService getRawExchangeService() {
        return service;
    }

}

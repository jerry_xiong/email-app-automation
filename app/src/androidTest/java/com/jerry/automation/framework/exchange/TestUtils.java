package com.jerry.automation.framework.exchange;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.test.InstrumentationRegistry;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import static org.junit.Assert.assertNotNull;

public class TestUtils {

    public final static int LAUNCH_TIMEOUT = 3000;
    public final static String TAG = "Automator";

    private static String appPackageName;
    private static UiDevice device;
    private static Context context;
    private static Properties testData = new Properties();

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

    private static final String AUTOMATION_DATA_FILE = "automation.properties";
    private static final String AUTOMATION_DATA_PATH = "jerrytool/automation/";

    static {
        init();
    }

    private static void init() {
        context = InstrumentationRegistry.getContext();
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        try {
            String state = Environment.getExternalStorageState();
            File externalProperties = null;
            if (state.equals(Environment.MEDIA_MOUNTED)) {
                File sdcard = Environment.getExternalStorageDirectory();
                externalProperties = new File(sdcard, AUTOMATION_DATA_PATH + AUTOMATION_DATA_FILE);
            }
            if (externalProperties != null && externalProperties.exists()) {
                testData.load(new FileInputStream(externalProperties));
                Log.d(TAG, "load external automation data file");
            } else {
                testData.load(context.getAssets().open(AUTOMATION_DATA_FILE));
                Log.d(TAG, "load internal automation data file");
            }
        } catch (FileNotFoundException e) {
            Log.e(TestUtils.TAG, "Init test data failed due to FileNotFoundException", e);
        } catch (IOException e) {
            Log.e(TestUtils.TAG, "Init test data failed due to IOException", e);
        }
        appPackageName = getTestData("TARGET_APP_PACKAGE");
    }

    public static Context getContext() {
        return context;
    }

    public static String getAppPackageName() {
        return appPackageName;
    }

    public static UiDevice getDevice() {
        return device;
    }

    public static void unlockGDIfNecessary() {

        getDevice().wait(Until.findObject(By.res(constructResourceId("COM_GOOD_GD_LOGIN_VIEW_PASSWORD_FIELD"))), LAUNCH_TIMEOUT);

        // Type unlock password and then press the Go button.
        UiObject passwordEdit = getUiObjectById("COM_GOOD_GD_LOGIN_VIEW_PASSWORD_FIELD");
        if (passwordEdit.exists()) {
            try {
                passwordEdit.setText(getTestData("UNLOCK_PASSWORD"));
            } catch (UiObjectNotFoundException e) {
                e.printStackTrace();
            }
        }
        UiObject loginButton = getUiObjectById("COM_GOOD_GD_EPROV_ACCESS_BUTTON");
        if (loginButton.exists()) {
            try {
                loginButton.clickAndWaitForNewWindow(LAUNCH_TIMEOUT);
            } catch (UiObjectNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public static void launchClient() {
        // Launch the target app
        Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager().getLaunchIntentForPackage(appPackageName);
        context.startActivity(intent);
    }

    // helper method
    // e.g com.good.gcs:id/specific_subject
    public static UiObject getUiObjectById(String resourceId) {
        return device.findObject(new UiSelector().resourceId(constructResourceId(resourceId)));
    }

    // helper method
    public static UiObject getUiObjectById(String resourceId, boolean appendPackagePrefix) {
        if (appendPackagePrefix) {
            return getUiObjectById(resourceId);
        }
        // e.g android:id/search_src_text
        return device.findObject(new UiSelector().resourceId(resourceId));
    }

    // helper method
    public static UiObject getUiObjectByText(String text) {
        return device.findObject(new UiSelector().text(text));
    }

    // helper method
    public static UiObject getUiObjectByTextContains(String text) {
        return device.findObject(new UiSelector().textContains(text));
    }

    // helper method
    public static UiObject getUiObjectByDesc(String desc) {
        return device.findObject(new UiSelector().descriptionContains(desc));
    }

    // helper method
    public static String constructResourceId(String pureId) {
        return appPackageName + ":id/" + pureId;
    }

    // helper method
    public static String getTestData(int keyId) {
        return InstrumentationRegistry.getContext().getString(keyId);
    }

    // helper method
    public static String getTestData(String keyId) {
        return testData.getProperty(keyId);
    }

    /**
     * pull down to refresh email list
     *
     * @throws UiObjectNotFoundException
     */
    public static void pullToRefresh() throws UiObjectNotFoundException {
        UiScrollable listView = new UiScrollable(new UiSelector().resourceId("android:id/list"));
        assertNotNull(listView);
        listView.swipeDown(5);
    }

    public static void dismissExtra() throws UiObjectNotFoundException {
        // dismissExtra the extra item if necessary
        UiScrollable listView = new UiScrollable(new UiSelector().resourceId("android:id/list"));
        UiObject dismissButton1 = listView.getChild(new UiSelector().resourceId(constructResourceId("dismiss_button")));
        if (dismissButton1.exists()) {
            dismissButton1.click();
        }

    }

    public static String getCurrentTimeTag() {
        return dateFormat.format(new Date());
    }

}

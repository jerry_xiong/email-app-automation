package com.jerry.automation.framework.exchange;

import android.support.test.runner.AndroidJUnit4;
import android.text.format.DateUtils;
import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import microsoft.exchange.webservices.data.core.enumeration.availability.AvailabilityData;
import microsoft.exchange.webservices.data.core.enumeration.property.EmailAddressKey;
import microsoft.exchange.webservices.data.core.enumeration.property.Importance;
import microsoft.exchange.webservices.data.core.enumeration.property.LegacyFreeBusyStatus;
import microsoft.exchange.webservices.data.core.enumeration.property.OofState;
import microsoft.exchange.webservices.data.core.enumeration.property.PhysicalAddressKey;
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.enumeration.service.DeleteMode;
import microsoft.exchange.webservices.data.core.response.AttendeeAvailability;
import microsoft.exchange.webservices.data.core.service.folder.Folder;
import microsoft.exchange.webservices.data.core.service.item.Appointment;
import microsoft.exchange.webservices.data.core.service.item.Contact;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.misc.availability.AttendeeInfo;
import microsoft.exchange.webservices.data.misc.availability.AvailabilityOptions;
import microsoft.exchange.webservices.data.misc.availability.GetUserAvailabilityResults;
import microsoft.exchange.webservices.data.misc.availability.OofReply;
import microsoft.exchange.webservices.data.misc.availability.TimeWindow;
import microsoft.exchange.webservices.data.property.complex.EmailAddress;
import microsoft.exchange.webservices.data.property.complex.MessageBody;
import microsoft.exchange.webservices.data.property.complex.PhysicalAddressEntry;
import microsoft.exchange.webservices.data.property.complex.availability.OofSettings;
import microsoft.exchange.webservices.data.search.FindFoldersResults;
import microsoft.exchange.webservices.data.search.FindItemsResults;

import static com.jerry.automation.framework.exchange.TestUtils.getCurrentTimeTag;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(AndroidJUnit4.class)
public class EwsTest {

    private IExchange exchange = null;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

    @Before
    public void setUp() {
        exchange = ServiceFactory.getExchangeService();
    }

    @Test
    public void createFolder() {
        try {
            assertEquals(0, exchange.findChildFolders(WellKnownFolderName.Inbox).getTotalCount());

            String newFolderName = "EWS-JAVA-Folder";
            exchange.createNewFolder(newFolderName, WellKnownFolderName.Inbox);
            assertEquals(1, exchange.findChildFolders(WellKnownFolderName.Inbox).getTotalCount());

            FindFoldersResults foldersResults = exchange.findChildFolders(WellKnownFolderName.Inbox);
            for (Folder folder : foldersResults.getFolders()) {
                if (folder.getDisplayName().equals(newFolderName)) {
                    folder.delete(DeleteMode.HardDelete);
                    break;
                }
            }
            assertEquals(0, exchange.findChildFolders(WellKnownFolderName.Inbox).getTotalCount());
        } catch (Exception e) {
            Log.e(TestUtils.TAG, "createFolder test failed", e);
            fail("createFolder test failed\n" + e.getMessage());
        }
    }

    @Test
    public void sendMail() {
        try {
            String subject = "EWS Android Testing " + getCurrentTimeTag();
            String body = "Sent using EWS Java API";
            EmailMessage emailMessage = exchange.createNewEmail();
            emailMessage.setSubject(subject);
            emailMessage.setBody(MessageBody.getMessageBodyFromText(body));
            emailMessage.getToRecipients().add("jexiong2@g3.qagood.com");
            emailMessage.getCcRecipients().add("jexiong1@g3.qagood.com");
            emailMessage.setImportance(Importance.Low);
            emailMessage.save();
            emailMessage.send();
            Thread.sleep(TestUtils.LAUNCH_TIMEOUT);
            FindItemsResults<Item> searchResult = exchange.findEmailItems(subject, body, WellKnownFolderName.Inbox);
            assertEquals(1, searchResult.getTotalCount());
        } catch (Exception e) {
            Log.e(TestUtils.TAG, "sendMail test failed", e);
            fail("sendMail test failed\n" + e.getMessage());
        }
    }

    @Test
    public void createContact() {
        try {
            Contact contact = exchange.createNewContact();
            String givenName = "Jerry " + getCurrentTimeTag();
            String surname = "Xiong";
            contact.setGivenName(givenName);
            contact.setSurname(surname);
            contact.setSubject("Contact Details");
            contact.setFileAs("Jerry Xiong");
            contact.setOfficeLocation("CNT");
            contact.setBirthday(new Date());
            contact.getEmailAddresses().setEmailAddress(EmailAddressKey.EmailAddress1,
                    EmailAddress.getEmailAddressFromString("jexiong1@g3.qagood.com"));
            Date birthDay = new Date();
            contact.setBirthday(birthDay);
            contact.setDepartment("Apps");
            contact.setNickName("Test");
            PhysicalAddressEntry paEntry1 = new PhysicalAddressEntry();
            paEntry1.setStreet("12345 Main Street");
            paEntry1.setCity("Tianjin");
            paEntry1.setPostalCode("300000");
            paEntry1.setCountryOrRegion("CHINA");
            contact.getPhysicalAddresses().setPhysicalAddress(PhysicalAddressKey.Home, paEntry1);
            contact.save();

            Thread.sleep(TestUtils.LAUNCH_TIMEOUT * 3);

            FindItemsResults<Item> findResults = exchange.findContactItems(givenName, surname);
            assertEquals("contact not found", 1, findResults.getTotalCount());
        } catch (Exception e) {
            Log.e(TestUtils.TAG, "createContact test failed", e);
            fail("createContact test failed\n" + e.getMessage());
        }
    }

    @Test
    public void createAppointment() {
        try {
            Appointment appointment = exchange.createNewAppointment();
            String subject = "Jerry Appointment " + getCurrentTimeTag();
            String location = "CNT";
            appointment.setSubject(subject);
            appointment.setLocation(location);
            appointment.setStart(new Date());
            appointment.setEnd(new Date(System.currentTimeMillis() + DateUtils.HOUR_IN_MILLIS));
            appointment.save();
            FindItemsResults<Item> findItemsResults = exchange.findCalendarItems(subject, location);
            assertEquals("calendar event not found", 1, findItemsResults.getTotalCount());
        } catch (Exception e) {
            Log.e(TestUtils.TAG, "createAppointment test failed", e);
            fail("createAppointment test failed\n" + e.getMessage());
        }
    }

    @Test
    public void createMeeting() {
        try {
            Appointment appointment = exchange.createNewAppointment();
            String subject = "Jerry Meeting " + getCurrentTimeTag();
            String location = "CNT";
            appointment.setSubject(subject);
            appointment.setLocation(location);
            appointment.setStart(new Date());
            appointment.setEnd(new Date(System.currentTimeMillis() + DateUtils.HOUR_IN_MILLIS));
            appointment.getRequiredAttendees().add("jexiong2@g3.qagood.com");
            appointment.save();
            FindItemsResults<Item> findItemsResults = exchange.findCalendarItems(subject, location);
            assertEquals("calendar event not found", 1, findItemsResults.getTotalCount());
        } catch (Exception e) {
            Log.e(TestUtils.TAG, "createMeeting test failed", e);
            fail("createMeeting test failed\n" + e.getMessage());
        }
    }

    @Test
    public void listInboxEmails() {
        try {
            FindItemsResults<Item> findItemsResults = exchange.findItems(WellKnownFolderName.Inbox);
            for (Item item : findItemsResults) {
                EmailMessage email = (EmailMessage) item;
                Log.d(TestUtils.TAG, "Inbox email: " + email.getSubject() + ", "
                        + email.getDateTimeReceived() + ", " + email.getSender());
            }
            assertTrue("inbox is empty", findItemsResults.getTotalCount() > 1);
        } catch (Exception e) {
            Log.e(TestUtils.TAG, "listInboxEmails test failed", e);
            fail("listInboxEmails test failed\n" + e.getMessage());
        }
    }

    @Test
    public void listCalendarEvents() {
        try {
            FindItemsResults<Item> findItemsResults = exchange.findItems(WellKnownFolderName.Calendar);
            for (Item item : findItemsResults) {
                Appointment appointment = (Appointment) item;
                Log.d(TestUtils.TAG, "Calendar event: " + appointment.getSubject() + ", "
                        + appointment.getLocation() + ", " + appointment.getOrganizer());
            }
            assertTrue("calendar is empty", findItemsResults.getTotalCount() > 1);
        } catch (Exception e) {
            Log.e(TestUtils.TAG, "listCalendarEvents test failed", e);
            fail("listCalendarEvents test failed\n" + e.getMessage());
        }
    }

    @Test
    public void listContacts() {
        try {
            FindItemsResults<Item> findItemsResults = exchange.findItems(WellKnownFolderName.Contacts);
            for (Item item : findItemsResults) {
                Contact contact = (Contact) item;
                Log.d(TestUtils.TAG, "Contact: " + contact.getDisplayName() + ", " + contact.getOfficeLocation());
            }
            assertTrue("contacts is empty", findItemsResults.getTotalCount() > 1);


        } catch (Exception e) {
            Log.e(TestUtils.TAG, "listContacts test failed", e);
            fail("listContacts test failed\n" + e.getMessage());
        }
    }

    @Test
    public void checkOutOfOffice() {
        String account = "jexiong1@g3.qagood.com";
        try {
            //TODO
            OofSettings oofSettings = exchange.getRawExchangeService().getUserOofSettings(account);
            assertEquals(OofState.Enabled, oofSettings.getState());

            oofSettings.setInternalReply(new OofReply("mock vacation reply"));
            Date startDate = dateFormat.parse("20151106180000");
            Date endDate = dateFormat.parse("20151107180000");
            oofSettings.setDuration(new TimeWindow(startDate, endDate));
            oofSettings.setState(OofState.Enabled);
            exchange.getRawExchangeService().setUserOofSettings(account, oofSettings);

            oofSettings = exchange.getRawExchangeService().getUserOofSettings(account);
            assertEquals(OofState.Enabled, oofSettings.getState());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getUserAvailability() {
        List<AttendeeInfo> attendeeInfoList = new ArrayList<>();
        attendeeInfoList.add(new AttendeeInfo("jexiong1@g3.qagood.com"));
        try {
            Date startDate = dateFormat.parse("20151106180000");
            Date endDate = dateFormat.parse("20151107180000");
            TimeWindow timeWindow = new TimeWindow(startDate, endDate);
            AvailabilityOptions availabilityOptions = new AvailabilityOptions();
            GetUserAvailabilityResults getUserAvailabilityResults = exchange.getRawExchangeService().getUserAvailability(attendeeInfoList, timeWindow,
                    AvailabilityData.FreeBusy, availabilityOptions);
            for (AttendeeAvailability attendeeAvailability : getUserAvailabilityResults.getAttendeesAvailability()) {
                for (LegacyFreeBusyStatus status : attendeeAvailability.getMergedFreeBusyStatus()) {
                    Log.d(TestUtils.TAG, "busy status: " + status);
                    //TODO
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
